# Ansible Ubuntu setup
Ansible roles to setup Ubuntu desktop and Ubuntu server (14.04 and 16.04). This playbook is focused on quickly deploying a "ready to use" dev machine.


## Requirements
- Git
- Ansible 2+ (automatically installed from [Ansible offical PPA](https://launchpad.net/~ansible/+archive/ubuntu/ansible) with the provided install.sh script)


## Installation
First, you need to install Git and Ansible :
```
sudo apt-get install git
git clone https://github.com/ivannieto/ansible-ubuntu.git
cd ansible-ubuntu
./install.sh
```

Then you need to customize the playbook `ansible-desktop.yml` (or create a new one) to suit your needs. Every roles are disabled by default.

Run `ansible-playbook ansible-desktop.yml --ask-become-pass` and enter your sudo password to run the playbook

## Roles included

| Role                     | Description   |
| ------------------------ | ------------- |
|                          |  **General**  |
| common                   | Install a lot of usefull packages (curl, htop, less, zip ... see corresponding task file)  |
| locales                  | Configure system locales and timezone |
| java-openjdk             | Install Default Java JDK               |
| codecs                   | Install variety of media codecs |
| ripgrep                  | Install [ripgrep](https://github.com/jimhester/ripgrep) (grep++)                            |
| sift                     | Install [Sift](https://sift-tool.org/) tool, a fast and powerful alternative to grep |
| zsh                      | Install [ZSH](http://www.zsh.org/) and create a [zshrc file] for the current user / root |
| | **Desktop tools** |
| arc-theme                | Install [Arc Theme](https://github.com/horst3180/arc-theme) from source and apply some gsettings |
| chromium                 | Install [Chromium](https://www.chromium.org/). May also install plugins and set policies                     |
| dbeaver                  | Install [DBeaver](http://dbeaver.jkiss.org/) from online deb file                     |
| desktop                  | Install a lot of usefull packages (meld, tilda, vlc, xclip)                           |
| desktop-autostart        | Update autostart handling (unhide all apps, create, remove...)                        |
| desktop-cleanup          | Remove Unity sh... integrations and install Nautilus plugins                          |
| desktop-preferences-unity | This one is very personal. Imports all my Unity preferences                          |
| filezilla                | Install [Filezilla](https://filezilla-project.org/) (no particular settings, basic installation)             |
| firefox                  | Install [Firefox](https://www.mozilla.org/firefox/) (no particular settings, basic installation)             |
| flatabulous              | Install [Flatabulous Theme](https://github.com/andreisergiu98/arc-flatabulous-theme) from source and apply some gsettings                                            |
| gimp                     | Install [Gimp](https://www.gimp.org/) and some minor settings                         |
| github                   | Download and authorize ssh keys from github account |
| grub-customizer          | Install Grub Customizer using [Daniel Richter PPA](https://launchpad.net/~danielrichter2007/+archive/ubuntu/grub-customizer)                                         |
| indicator-sysmonitor     | Install [indicator-sysmonitor](https://github.com/fossfreedom/indicator-sysmonitor) from [FOSSFreedom PPA](https://launchpad.net/~fossfreedom/+archive/ubuntu/indicator-sysmonitor)                                   |
| inkscape                 | Install [Inkscape](#) |
| libreoffice              | Install [LibreOffice](https://www.libreoffice.org/) using [LibreOffice 5.1 PPA](https://launchpad.net/~libreoffice/+archive/ubuntu/libreoffice-5-1)                  |
| mysql-workbench          | Install [MySQL WorkBench](https://www.mysql.fr/products/workbench/) from online deb file                     |
| nautilus-plugins         | Install Nautilus plugins               |
| notepadqq                | Install [Notepadqq](http://notepadqq.altervista.org/wp/) from [Notepadqq Team PPA](https://launchpad.net/~notepadqq-team/+archive/ubuntu/notepadqq)                  |
| notify-osd               | Install ehanced Notidy-OSD from [Leolik PPA](https://launchpad.net/~leolik/+archive/ubuntu/leolik)           |
| pandoc                   | Install pandoc General Markup Converter [Pandoc](http://pandoc.org/) |
| paper-icons              | Install [Paper-Icons Theme](https://snwh.org/paper) |
| powerline-fonts          | Install [Powerline Patched Fonts](https://github.com/powerline/fonts) |
| remarkable               | Install [Remarkable](https://remarkableapp.github.io/linux.html) from online deb file |
| remmina                  | Install [Remmina](http://www.remmina.org/)                                            |
| shutter                  | Install [Shutter](http://shutter-project.org/) screenshot tool                        |
| skype                    | Install [Skype](https://www.skype.com/)|
| slop                     | Install [slop (select operation)](https://github.com/naelstrof/slop) (selection tool) from [WebUpd8 Alin Andrei PPA](https://launchpad.net/~nilarimogard/+archive/ubuntu/webupd8)                                     |
| smartgit                 | Install [SmartGit](http://www.syntevo.com/smartgit/) from [Eugene San PPA](https://launchpad.net/~eugenesan/+archive/ubuntu/ppa)                                     |
| teamviewer               | Install [TeamViewer](https://www.teamviewer.com/) from online deb file                |
| tilix                    | Install [Tilix](https://gnunn1.github.io/tilix-web/)
| ultra-flat-icons         | Install [Ultra flat icons](http://www.noobslab.com/2015/01/make-linux-more-elegant-with-ultra-flat.html) from [Noobslab PPA](https://launchpad.net/~noobslab/+archive/ubuntu/icons)                                   |
| vagrant                  | Install [Vagrant](https://www.vagrantup.com/) from online deb file                    |
| virtualbox               | Install [VirtualBox](https://www.virtualbox.org/) from VirtualBox APT repositories    |
| visual-studio-code       | Install [Visual Studio Code](https://code.visualstudio.com/) |
| wine                     | Install [Wine](https://www.winehq.org/) from [Ubuntu Wine Team PPA](https://launchpad.net/~ubuntu-wine/+archive/ubuntu/ppa) and create [HeidiSQL](http://www.heidisql.com/) shortcut                                  |
| zeal                     | Install [Zeal](https://zealdocs.org) from Zeal APT repositories |
| | **Dev tools** |
| bower                    | Install [Bower](https://bower.io/) as a global package using NPM                      |
| browsersync              | Install [Browsersync](https://www.browsersync.io/) as a global package using NPM      |
| composer                 | Install [Composer](https://getcomposer.org/), PHP Dependency Manager                  |
| gulp                     | Install [Gulp](http://gulpjs.com/) as a global package using NPM                      |
| php-phar-tools           | Install common PHP tools (PHPUnit, Codeception, PHP-CS-Fixer, ...)                    |
| webpack                  | Install [webpack](https://webpack.github.io/docs/) as a global package using NPM      |
| | **Services & server tools** |
| apache2                  | Install [Apache 2 HTTP Server](https://httpd.apache.org/) with some admin tools and remove defaults hosts    |
| apache2-php              | Basic Apache 2 PHP Virtualhosts creation (you may need to run the "php" role first, depending on your configuration)                                                 |
| docker                   | Install [Docker](https://www.docker.com/) and Docker compose from Docker deb repository                      |
| fail2ban                 | Install [Fail2ban](http://www.fail2ban.org/) with default config                      |
| hhvm                     | Install [HHVM](http://hhvm.com/) from HHVM deb repository                             |
| memcached                | Install [Memcached](https://memcached.org/) with default config                       |
| mariadb                  | Install [Maria DB]() |
| mysql56                  | Install [MySQL 5.6](http://www.mysql.com/) (using 14.04 repositories on 16.04)        |
| mysql57                  | Install [MySQL 5.7](http://www.mysql.com/) **Only available on Ubuntu 16.04+**        |
| nginx                    | Install [nginx](https://nginx.org/) and remove defaults hosts                         |
| nginx-php-fpm            | Basic nginx PHP-FPM Virtualhosts creation (you may need to run the "php" role first, depending on your configuration)                                                |
| nodejs                   | Install [NodeJS](https://nodejs.org/en/) from Node deb repository                     |
| nodejs-fix-global-path   | Fix for global path for npm                                                           |
| phantomjs                | Install [PhantomJS](http://phantomjs.org/)                                            |
| php                      | Co-Install [PHP 5.6 and 7.0](http://php.net/) from [Ondřej Surý PPA](https://launchpad.net/~ondrej/+archive/ubuntu/php), with the "standard" set of extensions and settings     |
| php-pecl                 | Install PHP PECL extensions            |
| python                   | Install [Python](https://www.python.org/)                                             |
| redis                    | Install [Redis](http://redis.io/)      |
| ruby                     | Install [Ruby](https://www.ruby-lang.org/) from [Brightbox PPA](https://launchpad.net/~brightbox/+archive/ubuntu/ruby-ng)                                            |
| ssh                      | Install [OpenSSH Server](http://www.openssh.com/)                                     |


ToDo List:

- [ ] Add keepassx
- [ ] Deploy docker swarm
- [ ] Deploy portainer as a service Swarm GUI
- [ ] Add kubernetes
- [ ] Add opencv
